# Установка и настройка проекта Поток в docker

## Шаги

- Скачать проект с gitlab c ветки dev 
```sh
ssh git@git.tattelecom.digital:dev/potok.git
``` 
- Зайти в проект и установить все зависимости с помощью composer
```sh
composer install
``` 
- Попроси .env файл и установи его в папке /config
- Поменяй доменное имя в проекте на своeм компьютере в файле /config/dev/config.php на свой IP
- запусти сборку образов командой
```sh
docker compose up --build -d
``` 

## Как установить xdebug в проект
# P.S. IDE использовалась PHPSTORM

- Открываем IDE и заходим в settings
- Переходим во вкладку Debug и проверяем что у нас порт установлен на 9000, 9003
- Переходим во вкладку DBGp Proxy и устанавливаем 

| Name | Value |
| ------ | ------ |
| IDE Key | PHPSTORM|
| Port | 9000 |
 - Переходим во вкладку Servers устанавливаем host и порт
> Важно: `port = 81`
- Устнавливаем абсолютный путь до проекта в docker
 > Абсолютный путь: `/var/www/html`

